// rini <https://rinici.de>
//
// Copyright (c) 2023 rini
// SPDX-License-Identifier: AGPL-3.0

use std::{env, io};

use actix_files::Files;
use actix_logger::Logger;
use actix_web::{get, http::StatusCode, middleware::Compress, web, App, HttpServer, Responder};
use askama::Template;

#[derive(Template)]
#[template(path = "home.html")]
struct Home;

#[get("/")]
async fn home() -> impl Responder {
    Home
}

#[tokio::main]
async fn main() -> io::Result<()> {
    twink::log::setup();

    let port = env::var("PORT")
        .map(|p| p.parse().expect("invalid $PORT number"))
        .unwrap_or(8080);

    let server = HttpServer::new(|| {
        App::new()
            .wrap(Logger::new(twink::fmt!(
                "<green>%s <purple>%r</> took <cyan>%Dms</> | %{X-Forwarded-For}i <i>%{User-Agent}i</>"
            )))
            .wrap(Compress::default())
            .service(home)
            .service(Files::new("/", "static"))
            .default_service(web::to(|| async {
                "explode".customize().with_status(StatusCode::NOT_FOUND)
            }))
    });

    server.bind(("0.0.0.0", port))?.run().await
}
